import React, {useState, useEffect} from 'react';
import {ClassRocket, FunctionalRocket} from './Rocket';
import '../styles/_launchpad.scss';

export default function LaunchPad() {
  // const [, triggerRerender] = useState(0);

  // setTimeout(() => { triggerRerender(rerenderCount + 1); }, 500);
  const [, triggerRerender] = useState(Date.now());

  useEffect(() => {
    setInterval(() => {
      triggerRerender(Date.now());
    }, 300);
  }, []);

  return (
    <div className="launchpad">
      {/* <ClassRocket /> */}
      <FunctionalRocket />
    </div>
  );
}
